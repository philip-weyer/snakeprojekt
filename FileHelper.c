#include "FileHelper.h"
#include <stdio.h>


void saveScore(int score){
    FILE *file;

    file = fopen("score.txt", "w");
    fprintf(file, "%d", score);
    fclose(file);
}

int readScore(){
    int score;
    FILE *file;

    if((file = fopen("score.txt", "r"))==NULL){
        return 0;
    }

    fscanf(file, "%d", &score);
    fclose(file);
    return score;
}


