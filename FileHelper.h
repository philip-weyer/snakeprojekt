//
// Created by Philip Weyer on 15.01.21.
//

#ifndef SNAKEPROJEKT_FILEHELPER_H
#define SNAKEPROJEKT_FILEHELPER_H

void saveScore(int score);
int readScore();

#endif 
