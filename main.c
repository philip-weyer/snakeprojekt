#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <ncurses.h>
#include "Printing.h"
#include "FileHelper.h"
#include <stdlib.h>

#define LEFT -1
#define RIGHT 1
#define UP 2
#define DOWN -2
#define START 32
#define QUIT 113  


#define START_LENGTH 3


int main();
void resetSpielfeld();
void startMenu();
void *game();
void *gameInput();
void endGame();
void generateFood();
void input(int ch);
void moveSnake();
int validateMove();
int validateInput(int newDirection);

struct position {
	int x;
	int y;
};

struct position headPos, foodPos, tail;
struct position headHistory[100000];

int historyIndex;

int SPIELFELD[HEIGHT][WIDTH];

int direction, nextDirection, currentScore, highscore, LENGTH, GAME_RUNNING, PROGRAM_RUNNING, RUNNING = 1, GAMEOVER = 0, EXIT = -1;

int main() {
    PROGRAM_RUNNING = 1;
    highscore = readScore();

    initscr();
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    noecho();

    resetSpielfeld();

    while(PROGRAM_RUNNING){
        startMenu();
    }

    endwin();
    return 0;
}

void startMenu(){

    printMenu(highscore);
    int c = getch();

    if(c==START){

        pthread_t th1, th2;
        pthread_create(&th1, NULL, game, NULL);
        pthread_create(&th2, NULL, gameInput, NULL);
        pthread_join(th1, NULL);
        pthread_join(th2, NULL);

        if(GAME_RUNNING==GAMEOVER){
            printGameOver();
            sleep(2);
        }

        resetSpielfeld();
        
    }else if(c==QUIT){
        PROGRAM_RUNNING = 0;
    }
    
}

void *game(){
    while(GAME_RUNNING==RUNNING) {
        SPIELFELD[foodPos.y][foodPos.x] = FOOD;
        moveSnake();
        printSpielfeld(SPIELFELD, currentScore);
        if(direction==UP||direction==DOWN){
            usleep(250000);
        }else{
            usleep(150000);
        }
    }
    
}

void *gameInput(){
    while(GAME_RUNNING==RUNNING) { 
        int c = getch();
        if(c!=QUIT){
            input(c);
        }else{
            GAME_RUNNING = EXIT;
        }
        usleep(1000);
    }
}

void generateFood(){
    srand(time(NULL));
    do{
        foodPos.y = rand() % HEIGHT;
        foodPos.x = rand () % WIDTH;
    }while (SPIELFELD[foodPos.y][foodPos.x]==BODY);
}

void input(int ch){
    switch (ch) {
        case KEY_UP:
            if(validateInput(UP)){
                nextDirection = UP;
            }
            break;
        case KEY_DOWN:
            if(validateInput(DOWN)){
                nextDirection = DOWN;
            }
            break;
        case KEY_LEFT:
            if(validateInput(LEFT)){
                nextDirection = LEFT;
            }
            break;
        case KEY_RIGHT:
            if(validateInput(RIGHT)){
                nextDirection = RIGHT;
            }
            break;
    }
    
}

void moveSnake(){

    if(validateInput(nextDirection)){
        direction = nextDirection;
    }

    if(historyIndex-LENGTH>=0){
        SPIELFELD[headHistory[historyIndex-LENGTH].y][headHistory[historyIndex-LENGTH].x] = EMPTY;
    }

    SPIELFELD[headPos.y][headPos.x] = BODY;
    
    switch (direction){
        case RIGHT:
            if(headPos.x<WIDTH-1){
                headPos.x++;
            }else{
                headPos.x = 0;
            }
            break;
        case LEFT:
            if(headPos.x>0){
                headPos.x--;
            }else{
                headPos.x = WIDTH-1;
            }
            break;
        case UP:
            if(headPos.y>0){
                headPos.y--;
            }else{
                headPos.y = HEIGHT-1;
            }
            break;
        case DOWN:
            if(headPos.y<HEIGHT-1){
                headPos.y++;
            }else{
                headPos.y = 0;
            }
            break;
        default:
            break;
    }

    if(SPIELFELD[headPos.y][headPos.x]==BODY){
        saveScore(highscore);
        GAME_RUNNING = GAMEOVER;
    }else{
        if(SPIELFELD[headPos.y][headPos.x]==FOOD){
            currentScore++;
            if(currentScore>highscore){
                highscore = currentScore;
            }
            LENGTH += 3;
            generateFood();
        }
        SPIELFELD[headPos.y][headPos.x] = HEAD;
    }
    struct position tmp;
    tmp.y = headPos.y;
    tmp.x = headPos.x;
    
    headHistory[historyIndex+1] = tmp;
    historyIndex++;    
}

void resetSpielfeld(){
    GAME_RUNNING = 1;
    nextDirection = RIGHT;
    currentScore = 0;
    LENGTH = START_LENGTH;

    headPos.x=WIDTH/2;
	headPos.y=HEIGHT/2;

    for(int i=0; i<HEIGHT; i++){
        for(int k=0; k<WIDTH; k++){
            SPIELFELD[i][k] = EMPTY;
        }
    }
    SPIELFELD[headPos.y][headPos.x] = HEAD;
    headHistory[0] = headPos;
    historyIndex=0;

    generateFood();
}

int validateInput(int newDirection){
    return newDirection!=(direction*-1);
}

