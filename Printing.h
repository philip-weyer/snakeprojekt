
#ifndef SNAKEPROJEKT_PRINTING_H
#define SNAKEPROJEKT_PRINTING_H

#define EMPTY 0
#define HEAD 1
#define BODY 2
#define TAIL 3
#define FOOD 4

#define HEIGHT 20
#define WIDTH 60

void printSpielfeld(int SPIELFELD[HEIGHT][WIDTH], int score);
void printMenu(int score);
void printGameOver();

#endif
