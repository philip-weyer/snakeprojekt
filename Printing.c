#include "Printing.h"
#include <ncurses.h>

void printMenu(int score){
    clear();
    int py = 5, px = 4;

    mvprintw(py+0, px, "   ____ _  __ ___    __ __ ____");
    mvprintw(py+1, px, "  / __// |/ // _ |  / //_// __/");
    mvprintw(py+2, px, " _\\ \\ /    // __ | / ,<  / _/  ");
    mvprintw(py+3, px, "/___//_/|_//_/ |_|/_/|_|/___/  ");

    mvprintw(py+5, px, "HIGHSCORE : %d", score);
    mvprintw(py+7, px, "Leertaste um Spiel zu starten | 'q' um Programm zu beenden");
    
    refresh();
}

void printGameOver(){

    int py = (HEIGHT/2), px = 4;
    
    clear();
    mvprintw(py-3, px, "   _____            __  __  ______    ____ __      __ ______  _____  ");
    mvprintw(py-2, px, "  / ____|    /\\    |  \\/  ||  ____|  / __ \\\\ \\    / /|  ____||  __ \\ ");
    mvprintw(py-1, px, " | |  __    /  \\   | \\  / || |__    | |  | |\\ \\  / / | |__   | |__) |");
    mvprintw(py+0, px, " | | |_ |  / /\\ \\  | |\\/| ||  __|   | |  | | \\ \\/ /  |  __|  |  _  / ");
    mvprintw(py+1, px, " | |__| | / ____ \\ | |  | || |____  | |__| |  \\  /   | |____ | | \\ \\ ");
    mvprintw(py+2, px, "  \\_____|/_/    \\_\\|_|  |_||______|  \\____/    \\/    |______||_|  \\_\\");

    refresh();
}

void printSpielfeld(int SPIELFELD[HEIGHT][WIDTH], int score){
    mvprintw(0, 0, "'q' um Programm zu beenden | %d", score);

    for(int i=0; i<HEIGHT+2; i++){
        if(i==0||i==HEIGHT+1){
            for(int k=0; k<WIDTH+2; k++){
                mvaddch (i+2, k, '#');
            }
        }else{
            mvaddch (i+2, 0, '#');
            mvaddch (i+2, WIDTH+1, '#');
        }
    }

    for(int i=0; i<HEIGHT; i++){
        for(int k=0; k<WIDTH; k++){
            int part = SPIELFELD[i][k];
            char c;
            switch (part)
            {
                case EMPTY:
                    c = ' ';
                    break;
                case HEAD:
                    c = '0';
                    break;
                case BODY:
                    c = '*';
                    break;
                case FOOD:
                    c = '+';
                    break;
            }
            mvaddch (i+3, k+1, c);
        }
    } 
}

